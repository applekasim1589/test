//
//  CollectionViewCell.swift
//  NewFirstApp
//
//  Created by 周政翰 on 2017/5/23.
//  Copyright © 2017年 kasim. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView:UIImageView!
    @IBOutlet var labelView:UILabel!
    
    func setShowInformation(_ product:Product) {
        
        let url = URL(string: product.img!)
        let data = try? Data(contentsOf: url!)
        imageView.image = UIImage(data: data!)!
    }
    
}
