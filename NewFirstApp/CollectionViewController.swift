//
//  CollectionViewController.swift
//  NewFirstApp
//
//  Created by 周政翰 on 2017/5/22.
//  Copyright © 2017年 kasim. All rights reserved.
//

import UIKit


class CollectionViewController: UICollectionViewController {

    var products = [Product]()
    var index:Int8 = 0
    var labelIndex = 0
    var loading = false
    
    @IBOutlet var flowLayout : CollectionViewFlowLayout!
    
    var footerView:CollectionReusableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        loadData()
    }
    
    func loadData() {
        let dataOfProducts = Product.getDataForProducts(self.index)
        self.products.append(contentsOf: dataOfProducts as! [Product])
        self.index += 1
        
        self.flowLayout.columnCount = 3;
        self.flowLayout.products = self.products
        self.collectionView?.reloadData()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:-FooterView
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionFooter {
            self.footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterViewCache", for: indexPath) as? CollectionReusableView
        }
        return self.footerView!
    }


    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.footerView == nil || self.loading == true {
            return
        }
        
        
        print("y:\(scrollView.contentOffset.y)")
        print("height:\(scrollView.bounds.size.height)")
        /*
        if self.footerView!.frame.origin.y < (scrollView.contentOffset.y + scrollView.bounds.size.height) {
            
            self.loading = true
            self.footerView?.indicator.startAnimating()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
                self.footerView = nil
                self.loadData()
                self.loading = false
            })
        }*/
        
    }



}

//MARK:- UICollectionViewDataSource
extension CollectionViewController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! CollectionViewCell
        
        cell.setShowInformation(self.products[(indexPath as NSIndexPath).item])
        
        cell.labelView.text = String(labelIndex)
        labelIndex += 1
        
        return cell;
    }
}
