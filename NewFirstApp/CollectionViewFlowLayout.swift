//
//  CollectionViewFlowLayout.swift
//  NewFirstApp
//
//  Created by 周政翰 on 2017/5/23.
//  Copyright © 2017年 kasim. All rights reserved.
//

import UIKit

class CollectionViewFlowLayout: UICollectionViewFlowLayout {

    var products = [Product]()
    var columnCount:Int = 0
    
    fileprivate var layoutAttributesArrayForItems = [UICollectionViewLayoutAttributes]()
    
    override func prepare() {
        super.prepare()
        minimumInteritemSpacing = 10
        minimumLineSpacing      = 10
        
        sectionInset.top        = 10
        sectionInset.left       = 10
        sectionInset.right      = 10

        let contentWidth:CGFloat = (self.collectionView?.bounds.size.width)! - sectionInset.left - sectionInset.right
        let interitemSpacingSum = self.minimumLineSpacing * CGFloat.init(columnCount - 1)
        
        let itemWidth = (contentWidth - interitemSpacingSum) / CGFloat.init(columnCount)
        
        var columnsHeight = [CGFloat](repeating: CGFloat(self.sectionInset.top), count: self.columnCount)
        
        var columnItemCount = [Int](repeating: 0, count: self.columnCount)
        
        var attributesForItems = [UICollectionViewLayoutAttributes]()
        var itemIndex = 0
        for product in products {
            let indexPath = IndexPath.init(item: itemIndex, section: 0)
            let attributes = UICollectionViewLayoutAttributes.init(forCellWith: indexPath)
            let shortestColumnHeight = columnsHeight.sorted().first
            let columnIndex = columnsHeight.index(of: shortestColumnHeight!)
            
            columnItemCount[columnIndex!] += 1
            
            let itemX = (itemWidth + CGFloat(minimumLineSpacing)) * CGFloat(columnIndex!) + sectionInset.left
            
            let itemY = shortestColumnHeight! + CGFloat(minimumLineSpacing)
            let itemHeight = CGFloat(product.h) * (itemWidth / CGFloat(product.w))
            
            attributes.frame = CGRect(x: itemX, y: itemY, width: itemWidth, height: itemHeight)
            
            attributesForItems.append(attributes)
            itemIndex += 1
            
            columnsHeight[columnIndex!] = itemY + itemHeight
        }
        
        
        // 找出最高列列号
        let maxHeight = columnsHeight.sorted().last!
        let column = columnsHeight.index(of: maxHeight)

        // 根据最高列设置itemSize 使用总高度的平均值
        let itemH = (maxHeight - self.minimumLineSpacing * CGFloat(columnItemCount[column!])) / CGFloat(columnItemCount[column!])

        self.itemSize = CGSize(width: itemWidth, height: itemH )//CGSize(width: itemWidth, height: CGFloat(itemH))
        // 添加页脚属性
        let footerIndexPath:IndexPath = IndexPath.init(item: 0, section: 0)
        let footerAttr:UICollectionViewLayoutAttributes = UICollectionViewLayoutAttributes.init(forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, with: footerIndexPath)
        footerAttr.frame = CGRect(x: 0, y: CGFloat(maxHeight), width: self.collectionView!.bounds.size.width, height: 50)
        attributesForItems.append(footerAttr)
        
        
        self.layoutAttributesArrayForItems = attributesForItems
        
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return self.layoutAttributesArrayForItems
    }
}
